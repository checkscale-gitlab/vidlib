# VidLib - Video Library Stack

Download and manage tv-shows and movies

## Environments

**Example:**

```bash
#!/usr/bin/env
# environment variables for Docker-Compose
# Source: https://docs.docker.com/compose/environment-variables/#the-env-file
#
TZ=Europe/Zurich

# id username
PUID=1000
PGID=1000
UMASK_SET=022

NZBGET_HOST=nzbget.localdomain
SONARR_HOST=sonarr.localdomain
RADARR_HOST=radarr.localdomain
FILEBROWSER_HOST=filebrowser.localdomain

TRAEFIK_MIDDLEWARES=""

# openssh user: libvidadmin
USER_PASSWORD=verysecurepass
```

## Create NFS volumes

**Example:**

```bash
#!/bin/sh
#
# This is an example for an NFS volume
# 
VOLUME_NAME='downloads_data'
NFS_HOST_ADDR='0.0.0.0'
NFS_HOST_SHARE='/downloads_data'

echo "remove existing docker volume"
docker volume rm ${VOLUME_NAME}

echo "create external nfs4 volume "
docker volume create \
     --driver local \
      --opt type=nfs4 \
      --opt o=addr=${NFS_HOST_ADDR},rw,noatime,proto=tcp,sec=sys,rsize=8192,wsize=8192,timeo=600,hard \
      --opt device=:${NFS_HOST_SHARE} \
      ${VOLUME_NAME}
echo "mount and test created nfs volume"
docker run --interactive --tty --rm --name volumetest --mount source=${VOLUME_NAME},target=/volume_data alpine ls -al /volume_data
```
